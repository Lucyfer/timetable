package com.staremax.timetable.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.staremax.R;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.dao.TimetableDao;
import com.staremax.timetable.provider.iterator.PojoIterator;
import com.staremax.timetable.provider.mapper.TimetableMapper;
import com.staremax.timetable.provider.pojo.Timetable;

import java.util.ArrayList;
import java.util.List;

public class TimetableAdapter extends BaseAdapter {
    private final Context context;
    private final int week;
    private final int day;
    private List<Timetable> modelList;

    public TimetableAdapter(Context context, int week, int day) {
        this.context = context;
        this.week = week;
        this.day = day;
        this.modelList = new ArrayList<Timetable>();
        updateDataSource();
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Timetable getItem(int i) {
        return modelList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return getItem(i).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.timetable_list_item, parent, false);

        TextView beginTime = (TextView) rowView.findViewById(R.id.beginTime);
        TextView endTime = (TextView) rowView.findViewById(R.id.endTime);
        TextView subject = (TextView) rowView.findViewById(R.id.subject);
        TextView auditorium = (TextView) rowView.findViewById(R.id.auditorium);
        TextView teacher = (TextView) rowView.findViewById(R.id.teacher);

        Timetable model = getItem(position);
        beginTime.setText(model.lesson.beginTime);
        endTime.setText(model.lesson.endTime);
        subject.setText(model.subject.title);
        auditorium.setText(model.auditorium);
        teacher.setText(model.teacher.name);

        return rowView;
    }

    private void updateDataSource() {
        TimetableDao timetableDao = new TimetableDao(DatabaseHelperFactory.getHelper(context));
        modelList.clear();
        PojoIterator<Timetable> iterator = new PojoIterator<Timetable>(
                timetableDao.selectJoined(week, day), TimetableMapper.INSTANCE);
        for (Timetable item : iterator) {
            modelList.add(item);
        }
    }

    @Override
    public void notifyDataSetChanged() {
        updateDataSource();
        super.notifyDataSetChanged();
    }
}

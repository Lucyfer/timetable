package com.staremax.timetable.adapter;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.staremax.R;
import com.staremax.timetable.dialog.TimetableEditDialog;

public class GalleryAdapter extends BaseAdapter {
    private final String[] daysStringResource;
    private final String[] weeksStringResource;
    private final Context context;
    private static final int daysInTwoWeeks = 14;
    private final SparseArray<TimetableAdapter> timetableAdapters;
    private final AdapterView.OnItemLongClickListener itemLongClickListener;

    public GalleryAdapter(Context c) {
        this.daysStringResource = c.getResources().getStringArray(R.array.weekdays);
        this.weeksStringResource = c.getResources().getStringArray(R.array.weeknumbers);
        this.context = c;
        this.timetableAdapters = new SparseArray<TimetableAdapter>(daysInTwoWeeks);
        this.itemLongClickListener = new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long rowId) {
                TimetableEditDialog dialog = new TimetableEditDialog(context, rowId);
                dialog.show();
                return false;
            }
        };
    }

    @Override
    public int getCount() {
        return daysInTwoWeeks;
    }

    @Override
    public TimetableAdapter getItem(int i) {
        return timetableAdapters.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup parent) {
        final int week = i / 7;
        final int day = i % 7;
        if (timetableAdapters.get(i) == null) {
            timetableAdapters.put(i, new TimetableAdapter(context, week, day));
        }
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.main_list_item, parent, false);

        TextView weekTitle = (TextView) rowView.findViewById(R.id.weekTitle);
        TextView dayTitle  = (TextView) rowView.findViewById(R.id.dayTitle);
        ListView mainList  = (ListView) rowView.findViewById(R.id.mainListItem);

        weekTitle.setText(weeksStringResource[week]);
        dayTitle.setText(daysStringResource[day]);
        mainList.setAdapter(timetableAdapters.get(i));
        mainList.setOnItemLongClickListener(itemLongClickListener);

        return rowView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();

        for (int i = 0; i < daysInTwoWeeks; ++i) {
            TimetableAdapter t = timetableAdapters.get(i);
            if (t != null) t.notifyDataSetChanged();
        }
    }
}

package com.staremax.timetable.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Gallery;
import com.staremax.timetable.constants.ActivityConstants;
import com.staremax.R;
import com.staremax.timetable.adapter.GalleryAdapter;
import com.staremax.timetable.helper.CalendarHelper;
import com.staremax.timetable.helper.ContentChangeObserver;

import java.util.Observable;
import java.util.Observer;

public class MainActivity extends Activity implements Observer {
    private GalleryAdapter galleryAdapter = null;
    private Gallery mainGallery = null;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mainGallery = (Gallery) findViewById(R.id.mainGallery);
        galleryAdapter = new GalleryAdapter(this);
        mainGallery.setAdapter(galleryAdapter);
        SetTodayGalleryPosition();

        ContentChangeObserver.getInstance().addObserver(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        update(null, null);
    }

    private void SetTodayGalleryPosition() {
        int pos = CalendarHelper.getCurrentDay() + CalendarHelper.getCurrentWeek(this) * 7;
        mainGallery.setSelection(pos, true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ActivityConstants.CREATE_TIMETABLE_REQUEST:
            case ActivityConstants.EDIT_TIMETABLE_REQUEST:
                if (resultCode == RESULT_OK) {
                    // main work at onResume() now
                }
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goToGrid:
                Intent goToGrid = new Intent(this, MainGridActivity.class);
                this.startActivity(goToGrid);
                break;
            case R.id.today:
                SetTodayGalleryPosition();
                break;
            case R.id.newTimetableEntry:
                Intent newTimetableEntry = new Intent(this, TimetableItemActivity.class);
                this.startActivityForResult(newTimetableEntry, ActivityConstants.CREATE_TIMETABLE_REQUEST);
                break;
            case R.id.settings:
                Intent settingsIntent = new Intent(this, PrefActivity.class);
                this.startActivity(settingsIntent);
                break;
        }
        return true;
    }

    @Override
    public void update(Observable observable, Object o) {
        galleryAdapter.notifyDataSetChanged();
    }
}

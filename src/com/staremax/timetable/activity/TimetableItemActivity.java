package com.staremax.timetable.activity;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.staremax.R;
import com.staremax.timetable.helper.CalendarHelper;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.Metadata;
import com.staremax.timetable.provider.dao.SubjectDao;
import com.staremax.timetable.provider.dao.TeacherDao;
import com.staremax.timetable.provider.dao.TimetableDao;
import com.staremax.timetable.provider.iterator.PojoIterator;
import com.staremax.timetable.provider.mapper.TimetableMapper;
import com.staremax.timetable.provider.pojo.Lesson;
import com.staremax.timetable.provider.pojo.Subject;
import com.staremax.timetable.provider.pojo.Teacher;
import com.staremax.timetable.provider.pojo.Timetable;
import com.staremax.timetable.provider.validator.ValidationResult;

public class TimetableItemActivity extends Activity {
    private Timetable model;
    private TimetableDao timetableDao;
    private AutoCompleteTextView subjectTextView;
    private AutoCompleteTextView teacherTextView;
    private EditText auditoriumTextView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timetable_item);

        Bundle incomingData = getIntent().getExtras();
        if (incomingData == null)
            incomingData = new Bundle();

        timetableDao = new TimetableDao(DatabaseHelperFactory.getHelper(this));
        model = new PojoIterator<Timetable>(
                timetableDao.selectById(incomingData.getLong("id", -1L)),
                TimetableMapper.INSTANCE).iterator().next();

        final int day = (model != null) ? model.day : CalendarHelper.getCurrentDay();
        final int week = (model != null) ? model.week : CalendarHelper.getCurrentWeek(this);

        final String subject = (model != null) ? model.subject.title : "";
        final String teacher = (model != null) ? model.teacher.name : "";
        final String auditorium = (model != null) ? model.auditorium : "";

        if (model == null)
            model = new Timetable();

        final SubjectDao sDAO = new SubjectDao(DatabaseHelperFactory.getHelper(this));
        SimpleCursorAdapter subjectAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_spinner_item,
                sDAO.select(sDAO.getFields(), null, null, null),
                new String[]{Subject.TITLE},
                new int[]{android.R.id.text1});
        subjectAdapter.setStringConversionColumn(subjectAdapter.getCursor().getColumnIndex(Subject.TITLE));

        subjectTextView = (AutoCompleteTextView) findViewById(R.id.subjectTextView);
        subjectTextView.setAdapter(subjectAdapter);
        subjectTextView.setText(subject);

        ArrayAdapter<String> weeksAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.weeknumbers));
        weeksAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner weekSpinner = (Spinner) findViewById(R.id.weekSpinner);
        weekSpinner.setAdapter(weeksAdapter);
        weekSpinner.setSelection(week);
        weekSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                model.week = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        ArrayAdapter<String> daysAdapter = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_spinner_item,
                getResources().getStringArray(R.array.weekdays));
        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner daysSpinner = (Spinner) findViewById(R.id.daySpinner);
        daysSpinner.setAdapter(daysAdapter);
        daysSpinner.setSelection(day);
        daysSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                model.day = pos;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        SimpleCursorAdapter lessonAdapter = new SimpleCursorAdapter(
                this,
                R.layout.lesson_time,
                getContentResolver().query(Metadata.Lesson.CONTENT_URI, null, null, null, null),
                new String[]{Lesson.BEGIN_TIME, Lesson.END_TIME},
                new int[]{android.R.id.text1, android.R.id.text2});
        lessonAdapter.setDropDownViewResource(R.layout.simple_dropdown_item_2line);

        Spinner lessonSpinner = (Spinner) findViewById(R.id.lessonSpinner);
        lessonSpinner.setAdapter(lessonAdapter);
        lessonSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long id) {
                model.lesson.id = id;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        TeacherDao tDAO = new TeacherDao(DatabaseHelperFactory.getHelper(this));
        SimpleCursorAdapter teacherAdapter = new SimpleCursorAdapter(
                this,
                android.R.layout.simple_spinner_item,
                tDAO.select(tDAO.getFields(), null, null, null),
                new String[]{Teacher.NAME},
                new int[]{android.R.id.text1});
        teacherAdapter.setStringConversionColumn(teacherAdapter.getCursor().getColumnIndex(Teacher.NAME));

        teacherTextView = (AutoCompleteTextView) findViewById(R.id.teacherTextView);
        teacherTextView.setAdapter(teacherAdapter);
        teacherTextView.setText(teacher);

        auditoriumTextView = (EditText) findViewById(R.id.auditoriumTextView);
        auditoriumTextView.setText(auditorium);
    }

    public void saveBtnOnClick(View v) {
        model.subject.title = subjectTextView.getText().toString();
        model.teacher.name = teacherTextView.getText().toString();
        model.auditorium = auditoriumTextView.getText().toString();
        ValidationResult result = timetableDao.saveOrUpdateWithDependencies(model);
        if (result.isValid()) {
            setResult(RESULT_OK);
            finish();
        } else {
            Toast.makeText(this, TextUtils.join("\n", result.getErrorList()), Toast.LENGTH_SHORT).show();
        }
    }

    public void cancelBtnOnClick(View v) {
        setResult(RESULT_CANCELED);
        finish();
    }
}

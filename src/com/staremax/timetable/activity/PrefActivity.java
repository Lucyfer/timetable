package com.staremax.timetable.activity;

import android.os.Bundle;
import android.preference.PreferenceActivity;
import com.staremax.R;

public class PrefActivity extends PreferenceActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.preferences);
    }
}

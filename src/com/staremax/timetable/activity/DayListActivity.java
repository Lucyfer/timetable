package com.staremax.timetable.activity;

import android.app.ListActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;
import com.staremax.timetable.adapter.TimetableAdapter;
import com.staremax.timetable.helper.CalendarHelper;

public class DayListActivity extends ListActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle incomingData = getIntent().getExtras();
        if (incomingData == null)
            incomingData = new Bundle();

        int weeknum = incomingData.getInt("week", CalendarHelper.getCurrentWeek(this));
        int daynum  = incomingData.getInt("day", CalendarHelper.getCurrentDay());

        setListAdapter(new TimetableAdapter(this, weeknum, daynum));
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        long selectedID = getListAdapter().getItemId(position);
        Toast.makeText(this, Long.toString(selectedID) + " выбран", Toast.LENGTH_LONG).show();
    }
}

package com.staremax.timetable.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import com.staremax.timetable.constants.ActivityConstants;
import com.staremax.R;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.dao.LessonDao;
import com.staremax.timetable.provider.dao.TimetableDao;
import com.staremax.timetable.provider.iterator.PojoIterator;
import com.staremax.timetable.provider.mapper.LessonMapper;
import com.staremax.timetable.provider.mapper.TimetableMapper;
import com.staremax.timetable.provider.pojo.Lesson;
import com.staremax.timetable.provider.pojo.Timetable;

public class MainGridActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.main_grid);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateTables();
    }

    protected void updateTables() {
        loadViewForWeek(0, (TableLayout)findViewById(R.id.firstGrid));
        loadViewForWeek(1, (TableLayout)findViewById(R.id.secondGrid));
    }

    protected void loadViewForWeek(final int week, final TableLayout table) {
        final LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final TableRow.LayoutParams params = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

        CharSequence[] weekdays = getResources().getTextArray(R.array.weekdays);
        TableRow row = new TableRow(this);
        TextView space = (TextView)inflater.inflate(R.layout.main_grid_item, null);
        space.setText("");
        row.addView(space);
        for (int i = 0; i < 7; i++) {
            TextView header = (TextView)inflater.inflate(R.layout.main_grid_item, null);
            header.setLayoutParams(params);
            header.setText(weekdays[i]);
            row.addView(header);
        }
        table.removeAllViews();
        table.addView(row);

        SQLiteOpenHelper helper = DatabaseHelperFactory.getHelper(this);
        TimetableDao timetableDao = new TimetableDao(helper);
        LessonDao lessonDao = new LessonDao(helper);

        final Cursor lessonsCursor = lessonDao.select(lessonDao.getFields(), null, null, null);
        final PojoIterator<Lesson> iterator = new PojoIterator<Lesson>(lessonsCursor, LessonMapper.INSTANCE);
        for (Lesson lesson : iterator) {
            row = new TableRow(this);
            TextView rowHeader = (TextView)inflater.inflate(R.layout.main_grid_item, null);
            rowHeader.setText(lesson.beginTime);
            row.addView(rowHeader);

            for (int day = 0; day < 7; day++) {
                TextView text = (TextView)inflater.inflate(R.layout.main_grid_item, null);
                text.setLayoutParams(params);
                Timetable timetable = new PojoIterator<Timetable>(
                        timetableDao.selectJoinedWithLessonId(week, day, lesson.getId()),
                        TimetableMapper.INSTANCE).iterator().next();
                text.setText((timetable == null) ? "" : timetable.subject.toString());
                row.addView(text);
            }
            table.addView(row);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.grid_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goBack:
                finish();
                break;
            case R.id.newTimetableEntry:
                Intent newTimetableEntry = new Intent(this, TimetableItemActivity.class);
                this.startActivityForResult(newTimetableEntry, ActivityConstants.CREATE_TIMETABLE_REQUEST);
                break;
            case R.id.settings:
                Intent settingsIntent = new Intent(this, PrefActivity.class);
                this.startActivity(settingsIntent);
                break;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case ActivityConstants.CREATE_TIMETABLE_REQUEST:
                break;
        }
    }
}

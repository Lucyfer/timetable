package com.staremax.timetable.provider.pojo;

import com.staremax.timetable.provider.Metadata;

public class Subject implements Identificable, Metadata.Subject {
    public long id;
    public String title;

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return title;
    }
}

package com.staremax.timetable.provider.pojo;

import com.staremax.timetable.provider.Metadata;

public class Timetable implements Identificable, Metadata.Timetable {
    public long id;
    public int week;
    public int day;
    public String auditorium;

    public Lesson lesson;
    public Teacher teacher;
    public Subject subject;

    public long getId() {
        return id;
    }
}

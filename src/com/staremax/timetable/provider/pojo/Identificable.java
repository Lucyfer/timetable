package com.staremax.timetable.provider.pojo;

public interface Identificable {
    long getId();
}

package com.staremax.timetable.provider.pojo;

import com.staremax.timetable.provider.Metadata;

public class Lesson implements Identificable, Metadata.Lesson {
    public long id;
    public String beginTime;
    public String endTime;

    public long getId() {
        return id;
    }
}

package com.staremax.timetable.provider.pojo;

import com.staremax.timetable.provider.Metadata;

public class Teacher implements Identificable, Metadata.Teacher {
    public long id;
    public String name;

    public long getId() {
        return id;
    }

    @Override
    public String toString() {
        return name;
    }
}

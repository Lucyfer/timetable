package com.staremax.timetable.provider.mapper;

import android.content.ContentValues;
import android.database.Cursor;
import com.staremax.timetable.helper.DAOBindingHelper;
import com.staremax.timetable.provider.pojo.Subject;

public class SubjectMapper implements IPojoMapper<Subject> {
    public static SubjectMapper INSTANCE = new SubjectMapper();

    private SubjectMapper() {}

    @Override
    public Subject bind(Cursor c) {
        Subject subject = new Subject();
        subject.id = DAOBindingHelper.bindLong(c, Subject._ID);
        subject.title = DAOBindingHelper.bindString(c, Subject.TITLE);
        return subject;
    }

    @Override
    public ContentValues unbind(Subject model) {
        ContentValues values = new ContentValues();
        values.put(Subject.TITLE, model.title);
        return values;
    }
}

package com.staremax.timetable.provider.mapper;

import android.content.ContentValues;
import android.database.Cursor;
import com.staremax.timetable.helper.DAOBindingHelper;
import com.staremax.timetable.provider.pojo.Lesson;

public class LessonMapper implements IPojoMapper<Lesson> {
    public static LessonMapper INSTANCE = new LessonMapper();

    private LessonMapper() {
    }

    @Override
    public Lesson bind(Cursor c) {
        Lesson lesson = new Lesson();
        lesson.id = DAOBindingHelper.bindLong(c, Lesson._ID);
        lesson.beginTime = DAOBindingHelper.bindString(c, Lesson.BEGIN_TIME);
        lesson.endTime = DAOBindingHelper.bindString(c, Lesson.END_TIME);
        return lesson;
    }

    @Override
    public ContentValues unbind(Lesson model) {
        ContentValues values = new ContentValues();
        values.put(Lesson.BEGIN_TIME, model.beginTime);
        values.put(Lesson.END_TIME, model.endTime);
        return values;
    }
}

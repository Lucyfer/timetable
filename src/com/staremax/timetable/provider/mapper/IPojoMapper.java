package com.staremax.timetable.provider.mapper;

import android.content.ContentValues;
import android.database.Cursor;

public interface IPojoMapper<P> {
    P bind(Cursor c);
    ContentValues unbind(P model);
}

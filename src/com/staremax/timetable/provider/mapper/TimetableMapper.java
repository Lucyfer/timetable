package com.staremax.timetable.provider.mapper;

import android.content.ContentValues;
import android.database.Cursor;
import com.staremax.timetable.helper.DAOBindingHelper;
import com.staremax.timetable.provider.pojo.Lesson;
import com.staremax.timetable.provider.pojo.Subject;
import com.staremax.timetable.provider.pojo.Teacher;
import com.staremax.timetable.provider.pojo.Timetable;

public class TimetableMapper implements IPojoMapper<Timetable> {
    public static TimetableMapper INSTANCE = new TimetableMapper();

    private TimetableMapper() {}

    @Override
    public Timetable bind(Cursor c) {
        final String LESSON_PREFIX = "L.";
        final String TEACHER_PREFIX = "T.";
        final String SUBJECT_PREFIX = "S.";

        Timetable timetable = new Timetable();
        timetable.id = DAOBindingHelper.bindLong(c, Timetable._ID);
        timetable.week = DAOBindingHelper.bindInt(c, Timetable.WEEK);
        timetable.day = DAOBindingHelper.bindInt(c, Timetable.DAY);
        timetable.auditorium = DAOBindingHelper.bindString(c, Timetable.AUDITORIUM);

        timetable.lesson = new Lesson();
        timetable.lesson.id = DAOBindingHelper.bindLong(c, Timetable.LESSON_ID);
        timetable.lesson.beginTime = DAOBindingHelper.bindStringOption(c, LESSON_PREFIX + Lesson.BEGIN_TIME);
        timetable.lesson.endTime = DAOBindingHelper.bindStringOption(c, LESSON_PREFIX + Lesson.END_TIME);

        timetable.teacher = new Teacher();
        timetable.teacher.id = DAOBindingHelper.bindLong(c, Timetable.TEACHER_ID);
        timetable.teacher.name = DAOBindingHelper.bindStringOption(c, TEACHER_PREFIX + Teacher.NAME);

        timetable.subject = new Subject();
        timetable.subject.id = DAOBindingHelper.bindLong(c, Timetable.SUBJECT_ID);
        timetable.subject.title = DAOBindingHelper.bindStringOption(c, SUBJECT_PREFIX + Subject.TITLE);

        return timetable;
    }

    @Override
    public ContentValues unbind(Timetable model) {
        ContentValues values = new ContentValues();
        values.put(Timetable.WEEK, model.week);
        values.put(Timetable.DAY, model.day);
        if (model.auditorium == null)
            model.auditorium = "";
        values.put(Timetable.AUDITORIUM, model.auditorium);
        if (model.lesson.id > 0L)
            values.put(Timetable.LESSON_ID, model.lesson.id);
        if (model.teacher.id > 0L)
            values.put(Timetable.TEACHER_ID, model.teacher.id);
        if (model.subject.id > 0L)
            values.put(Timetable.SUBJECT_ID, model.subject.id);
        return values;
    }
}

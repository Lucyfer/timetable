package com.staremax.timetable.provider.mapper;

import android.content.ContentValues;
import android.database.Cursor;
import com.staremax.timetable.helper.DAOBindingHelper;
import com.staremax.timetable.provider.pojo.Teacher;

public class TeacherMapper implements IPojoMapper<Teacher> {
    public static TeacherMapper INSTANCE = new TeacherMapper();

    private TeacherMapper() {}

    @Override
    public Teacher bind(Cursor c) {
        Teacher teacher = new Teacher();
        teacher.id = DAOBindingHelper.bindLong(c, Teacher._ID);
        teacher.name = DAOBindingHelper.bindString(c, Teacher.NAME);
        return teacher;
    }

    @Override
    public ContentValues unbind(Teacher model) {
        ContentValues values = new ContentValues();
        values.put(Teacher.NAME, model.name);
        return values;
    }
}

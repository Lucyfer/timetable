package com.staremax.timetable.provider.dao;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.staremax.timetable.provider.Metadata;

public class LessonDao extends AbstractDao implements Metadata.Lesson {
    public LessonDao(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getFields() {
        return TABLE_FIELDS;
    }

    @Override
    public String[] getFieldsTypes() {
        return TABLE_FIELDS_TYPES;
    }

    public void bootstrap(SQLiteDatabase database) {
        final String[] beginTimes = {"08:15", "09:55", "11:35", "13:35", "15:15", "16:55", "18:35", "20:15"};
        final String[] endTimes = {"09:45", "11:25", "13:05", "15:05", "16:45", "18:25", "20:05", "21:45"};
        for (int i = 0; i < beginTimes.length; ++i) {
            insertRow(database, beginTimes[i], endTimes[i]);
        }
    }

    private void insertRow(SQLiteDatabase database, String beginTime, String endTime) {
        ContentValues cv = new ContentValues();
        cv.put(BEGIN_TIME, beginTime);
        cv.put(END_TIME, endTime);
        database.insert(getTableName(), null, cv);
    }
}

package com.staremax.timetable.provider.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.text.TextUtils;
import com.staremax.R;
import com.staremax.timetable.provider.Metadata;
import com.staremax.timetable.provider.iterator.PojoIterator;
import com.staremax.timetable.provider.mapper.SubjectMapper;
import com.staremax.timetable.provider.mapper.TeacherMapper;
import com.staremax.timetable.provider.mapper.TimetableMapper;
import com.staremax.timetable.provider.pojo.Subject;
import com.staremax.timetable.provider.pojo.Teacher;
import com.staremax.timetable.provider.pojo.Timetable;
import com.staremax.timetable.provider.validator.TimetableValidator;
import com.staremax.timetable.provider.validator.ValidationResult;

import java.util.ArrayList;
import java.util.List;

public class TimetableDao extends AbstractDao implements Metadata.Timetable {
    public TimetableDao(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getFields() {
        return TABLE_FIELDS;
    }

    @Override
    public String[] getFieldsTypes() {
        return TABLE_FIELDS_TYPES;
    }

    public Cursor selectJoined(int week, int day) {
        StringBuilder projection = getProjection();
        projection.append(" WHERE weeknum=? AND daynum=?");
        return dbHelper.getWritableDatabase().rawQuery(
                projection.toString(),
                new String[]{Integer.toString(week), Integer.toString(day)});
    }

    public Cursor selectJoinedWithLessonId(int week, int day, long lessonId) {
        StringBuilder projection = getProjection();
        projection.append(" WHERE weeknum=? AND daynum=? AND lesson_id=?");
        return dbHelper.getWritableDatabase().rawQuery(
                projection.toString(),
                new String[]{Integer.toString(week), Integer.toString(day), Long.toString(lessonId)});
    }

    private StringBuilder getProjection() {
        StringBuilder projection = new StringBuilder("SELECT");
        projection.append(" _id, auditorium, weeknum, daynum, lesson_id, teacher_id, subject_id,");
        projection.append(" L.begintime, L.endtime,");
        projection.append(" T.name, S.title");
        projection.append(" FROM timetable");
        projection.append(" LEFT JOIN lesson L ON lesson_id=L._id");
        projection.append(" LEFT JOIN teacher T ON teacher_id=T._id");
        projection.append(" LEFT JOIN subject S ON subject_id=S._id");
        return projection;
    }

    public ValidationResult saveOrUpdateWithDependencies(Timetable model) {
        TeacherDao tDao = new TeacherDao(dbHelper);
        Cursor teachersCursor = tDao.select(
                tDao.getFields(), Teacher.NAME + "=?", new String[]{model.teacher.name}, null);
        if (teachersCursor.getCount() > 0) {
            model.teacher.id = new PojoIterator<Teacher>(
                    teachersCursor, TeacherMapper.INSTANCE).iterator().next().getId();
        } else
            model.teacher.id = TextUtils.isEmpty(model.teacher.name)
                    ? 0L : tDao.insert(TeacherMapper.INSTANCE.unbind(model.teacher));
        SubjectDao sDao = new SubjectDao(dbHelper);
        Cursor subjectsCursor = sDao.select(
                sDao.getFields(), Subject.TITLE + "=?", new String[]{model.subject.title}, null);
        if (subjectsCursor.getCount() > 0)
            model.subject.id = new PojoIterator<Subject>(
                    subjectsCursor, SubjectMapper.INSTANCE).iterator().next().getId();
        else
            model.subject.id = sDao.insert(SubjectMapper.INSTANCE.unbind(model.subject));
        ValidationResult result = TimetableValidator.INSTANCE.validate(model);
        if (!result.isValid())
            return result;
        if (model.id <= 0L) {
            if (insert(TimetableMapper.INSTANCE.unbind(model)) > 0L)
                return ValidationResult.CORRECT;
        } else {
            if (updateById(model.id, TimetableMapper.INSTANCE.unbind(model)) > 0)
                return ValidationResult.CORRECT;
        }
        List<String> errors = new ArrayList<String>();
        errors.add("Database error");
        return new ValidationResult(errors);
    }
}

package com.staremax.timetable.provider.dao;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public abstract class AbstractDao {
    protected final SQLiteOpenHelper dbHelper;

    public AbstractDao(SQLiteOpenHelper helper) {
        this.dbHelper = helper;
    }

    public abstract String getTableName();

    public abstract String[] getFields();

    public abstract String[] getFieldsTypes();

    public void createTable(SQLiteDatabase database) {
        StringBuilder sb = new StringBuilder(100);
        sb.append('(').append(getFields()[0]);
        sb.append(' ').append(getFieldsTypes()[0]);
        for (int i = 1; i < getFields().length; ++i) {
            sb.append(',').append(getFields()[i]);
            sb.append(' ').append(getFieldsTypes()[i]);
        }
        sb.append(')');
        database.execSQL("DROP TABLE IF EXISTS " + getTableName());
        database.execSQL("CREATE TABLE " + getTableName() + sb);
    }

    public Cursor select(String[] columns, String where, String[] whereArgs, String orderBy) {
        return dbHelper.getReadableDatabase().query(getTableName(),
                columns, where, whereArgs, null, null, orderBy);
    }

    public Cursor selectById(long id) {
        return dbHelper.getReadableDatabase().query(getTableName(),
                getFields(), "_id = ?", new String[]{String.valueOf(id)}, null, null, null);
    }

    public Cursor selectWithId(long id, String[] columns, String where, String[] whereArgs, String orderBy) {
        return dbHelper.getReadableDatabase().query(getTableName(),
                columns, addIdToWhereClause(id, where), whereArgs, null, null, orderBy);
    }

    public int delete(String where, String[] whereArgs) {
        return dbHelper.getWritableDatabase().delete(getTableName(), where, whereArgs);
    }

    public int deleteById(long id) {
        return delete("_id = ?", new String[]{String.valueOf(id)});
    }

    public int update(ContentValues cv, String where, String[] whereArgs) {
        return dbHelper.getReadableDatabase().update(getTableName(), cv, where, whereArgs);
    }

    public int updateById(long id, ContentValues cv) {
        return update(cv, "_id = ?", new String[]{String.valueOf(id)});
    }

    public long insert(ContentValues cv) {
        return dbHelper.getWritableDatabase().insert(getTableName(), null, cv);
    }

    private String addIdToWhereClause(long id, String where) {
        where = (where == null) ? "" : where.trim();
        return where.concat((where.isEmpty() ? "" : " AND ") + "_id=" + id);
    }
}

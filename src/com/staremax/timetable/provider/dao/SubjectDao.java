package com.staremax.timetable.provider.dao;

import android.database.sqlite.SQLiteOpenHelper;
import com.staremax.timetable.provider.Metadata;

public class SubjectDao extends AbstractDao implements Metadata.Subject {
    public SubjectDao(SQLiteOpenHelper helper) {
        super(helper);
    }

    @Override
    public String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public String[] getFields() {
        return TABLE_FIELDS;
    }

    @Override
    public String[] getFieldsTypes() {
        return TABLE_FIELDS_TYPES;
    }
}

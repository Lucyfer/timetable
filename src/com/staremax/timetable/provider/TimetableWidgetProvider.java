package com.staremax.timetable.provider;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;
import com.staremax.R;
import com.staremax.timetable.service.TimetableUpdateService;

public class TimetableWidgetProvider extends AppWidgetProvider {
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget);

            Intent intent = new Intent(context, TimetableUpdateService.class);
            intent.setAction(TimetableUpdateService.UPDATE_TIMETABLE);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
            PendingIntent pendingIntent = PendingIntent.getService(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
            views.setOnClickPendingIntent(R.id.widgetBtn, pendingIntent);
            appWidgetManager.updateAppWidget(appWidgetId, views);
            try {
                pendingIntent.send();
            }
            catch (PendingIntent.CanceledException e) {
                Log.e(TimetableWidgetProvider.class.toString(), e.getMessage());
            }
        }
    }
}

package com.staremax.timetable.provider;

import android.content.UriMatcher;
import android.net.Uri;

class TimetableUriMatcher {
    public static final int TEACHER_COLLECTION = 1;
    public static final int TEACHER_ITEM = 2;
    public static final int TIMETABLE_COLLECTION = 3;
    public static final int TIMETABLE_ITEM = 4;
    public static final int SUBJECT_COLLECTION = 5;
    public static final int SUBJECT_ITEM = 6;
    public static final int LESSON_COLLECTION = 7;
    public static final int LESSON_ITEM = 8;

    private static final UriMatcher uriMatcher;

    private TimetableUriMatcher() {}

    static {
        // URI Matching
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Teacher.TABLE_NAME,
                TEACHER_COLLECTION);
        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Teacher.TABLE_NAME + "/#",
                TEACHER_ITEM);

        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Timetable.TABLE_NAME,
                TIMETABLE_COLLECTION);
        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Timetable.TABLE_NAME + "/#",
                TIMETABLE_ITEM);

        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Subject.TABLE_NAME,
                SUBJECT_COLLECTION);
        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Subject.TABLE_NAME + "/#",
                SUBJECT_ITEM);

        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Lesson.TABLE_NAME,
                LESSON_COLLECTION);
        uriMatcher.addURI(
                Metadata.AUTHORITY,
                Metadata.Lesson.TABLE_NAME + "/#",
                LESSON_ITEM);
    }

    public static int matchUri(Uri uri) {
        return uriMatcher.match(uri);
    }

    public static String getType(Uri uri) {
        switch (uriMatcher.match(uri)) {
            case TEACHER_COLLECTION:    return Metadata.Teacher.CONTENT_TYPE;
            case TEACHER_ITEM:          return Metadata.Teacher.CONTENT_ITEM_TYPE;
            case TIMETABLE_COLLECTION:  return Metadata.Timetable.CONTENT_TYPE;
            case TIMETABLE_ITEM:        return Metadata.Timetable.CONTENT_ITEM_TYPE;
            case SUBJECT_COLLECTION:    return Metadata.Subject.CONTENT_TYPE;
            case SUBJECT_ITEM:          return Metadata.Subject.CONTENT_ITEM_TYPE;
            case LESSON_COLLECTION:     return Metadata.Lesson.CONTENT_TYPE;
            case LESSON_ITEM:           return Metadata.Lesson.CONTENT_ITEM_TYPE;
            default:                    throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }
}

package com.staremax.timetable.provider.validator;

public interface IPojoValidator<P> {
    ValidationResult validate(P model);
}

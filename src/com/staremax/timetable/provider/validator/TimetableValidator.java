package com.staremax.timetable.provider.validator;

import com.staremax.timetable.provider.pojo.Timetable;

import java.util.ArrayList;

public class TimetableValidator implements IPojoValidator<Timetable> {
    public static TimetableValidator INSTANCE = new TimetableValidator();

    private TimetableValidator() {}

    @Override
    public ValidationResult validate(Timetable model) {
        ArrayList<String> errorList = new ArrayList<String>(4);
        if (model.week < 0 || model.week > 1)
            errorList.add("Incorrect value for week field");
        if (model.day < 0 || model.day > 6)
            errorList.add("Incorrect value for weekday field");
        if (model.lesson.id < 1L)
            errorList.add("Check lesson's value");
        if (model.subject.id < 1L)
            errorList.add("Check subject's value");
        return new ValidationResult(errorList);
    }
}

package com.staremax.timetable.provider.validator;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    public static ValidationResult CORRECT = new ValidationResult(new ArrayList<String>());

    protected final List<String> errorList;

    public ValidationResult(List<String> errorList) {
        this.errorList = errorList;
    }

    public boolean isValid() {
        return errorList.isEmpty();
    }

    public List<String> getErrorList() {
        return errorList;
    }
}

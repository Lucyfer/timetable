package com.staremax.timetable.provider.iterator;

import android.database.Cursor;
import com.staremax.timetable.provider.mapper.IPojoMapper;

import java.util.Iterator;

public class PojoIterator<P> implements Iterable<P> {
    protected final Cursor cursor;
    protected final IPojoMapper<P> mapper;

    public PojoIterator(Cursor cursor, IPojoMapper<P> mapper) {
        this.cursor = cursor;
        this.mapper = mapper;
    }

    @Override
    public Iterator<P> iterator() {
        return new Iterator<P>() {
            @Override
            public boolean hasNext() {
                return !cursor.isAfterLast();
            }

            @Override
            public P next() {
                if (cursor.moveToNext())
                    return mapper.bind(cursor);
                else
                    return null;
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException("Can't remove cursor's item");
            }
        };
    }
}

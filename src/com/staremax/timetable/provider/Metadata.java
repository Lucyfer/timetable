package com.staremax.timetable.provider;

import android.net.Uri;
import android.provider.BaseColumns;

public class Metadata {
    public static final String AUTHORITY = "com.staremax.timetable.provider";

    private Metadata() {}

    public interface Teacher extends BaseColumns {
        String NAME = "name";
        String[] TABLE_FIELDS = {_ID, NAME};
        String[] TABLE_FIELDS_TYPES = {"integer primary key autoincrement", "text"};

        String TABLE_NAME = "teacher";
        Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + '/' + TABLE_NAME);
        String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.staremax.timetable.teacher";
        String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.staremax.timetable.teacher";
    }

    public interface Timetable extends BaseColumns {
        String WEEK = "weeknum";
        String DAY = "daynum";
        String AUDITORIUM = "auditorium";
        String LESSON_ID = "lesson_id";
        String TEACHER_ID = "teacher_id";
        String SUBJECT_ID = "subject_id";
        String[] TABLE_FIELDS = {_ID, WEEK, DAY, AUDITORIUM,
                LESSON_ID, TEACHER_ID, SUBJECT_ID};
        String[] TABLE_FIELDS_TYPES = {"integer primary key autoincrement", "integer", "integer", "text",
                "integer REFERENCES " + Lesson.TABLE_NAME + "(_id)",
                "integer REFERENCES " + Teacher.TABLE_NAME + "(_id)",
                "integer REFERENCES " + Subject.TABLE_NAME + "(_id)"};

        String TABLE_NAME = "timetable";
        Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + '/' + TABLE_NAME);
        String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.staremax.timetable.entry";
        String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.staremax.timetable.entry";
    }

    public interface Lesson extends BaseColumns {
        String BEGIN_TIME = "begintime";
        String END_TIME = "endtime";
        String[] TABLE_FIELDS = {_ID, BEGIN_TIME, END_TIME};
        String[] TABLE_FIELDS_TYPES = {"integer primary key autoincrement", "text", "text"};

        String TABLE_NAME = "lesson";
        Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + '/' + TABLE_NAME);
        String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.staremax.timetable.lesson";
        String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.staremax.timetable.lesson";
    }

    public interface Subject extends BaseColumns {
        String TITLE = "title";
        String[] TABLE_FIELDS = {_ID, TITLE};
        String[] TABLE_FIELDS_TYPES = {"integer primary key autoincrement", "text"};

        String TABLE_NAME = "subject";
        Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + '/' + TABLE_NAME);
        String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.staremax.timetable.subject";
        String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.staremax.timetable.subject";
    }
}

package com.staremax.timetable.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.dao.*;

public class TimetableProvider extends ContentProvider {
    private TeacherDao teacherDao;
    private TimetableDao timetableDao;
    private SubjectDao subjectDao;
    private LessonDao lessonDao;

    @Override
    public boolean onCreate() {
        SQLiteOpenHelper dbHelper = DatabaseHelperFactory.getHelper(getContext());
        teacherDao = new TeacherDao(dbHelper);
        timetableDao = new TimetableDao(dbHelper);
        subjectDao = new SubjectDao(dbHelper);
        lessonDao = new LessonDao(dbHelper);
        return true;
    }

    @Override
    public String getType(Uri uri) {
        return TimetableUriMatcher.getType(uri);
    }

    @Override
    public Cursor query(Uri uri, String[] columns, String where, String[] whereArgs, String orderBy) {
        Cursor result;
        switch (TimetableUriMatcher.matchUri(uri)) {
            case TimetableUriMatcher.TEACHER_COLLECTION:
                result = teacherDao.select(columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.TEACHER_ITEM:
                long tId = Long.getLong(uri.getPathSegments().get(1), 0L);
                result = teacherDao.selectWithId(tId, columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.TIMETABLE_COLLECTION:
                result = timetableDao.select(columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.TIMETABLE_ITEM:
                long ttId = Long.getLong(uri.getPathSegments().get(1), 0L);
                result = timetableDao.selectWithId(ttId, columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.SUBJECT_COLLECTION:
                result = subjectDao.select(columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.SUBJECT_ITEM:
                long sId = Long.getLong(uri.getPathSegments().get(1), 0L);
                result = subjectDao.selectWithId(sId, columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.LESSON_COLLECTION:
                result = lessonDao.select(columns, where, whereArgs, orderBy);
                break;
            case TimetableUriMatcher.LESSON_ITEM:
                long lId = Long.getLong(uri.getPathSegments().get(1), 0L);
                result = lessonDao.selectWithId(lId, columns, where, whereArgs, orderBy);
                break;
            default: throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        result.setNotificationUri(getContext().getContentResolver(), uri);
        return result;
    }

    private static Uri appendToContentUri(Uri baseUri, long rowId) {
        return ContentUris.withAppendedId(baseUri, rowId);
    }

    @Override
    public Uri insert(Uri uri, ContentValues contentValues) {
        Uri insertedItemUri;
        switch (TimetableUriMatcher.matchUri(uri)) {
            case TimetableUriMatcher.TEACHER_COLLECTION:
                insertedItemUri = appendToContentUri(Metadata.Teacher.CONTENT_URI,
                        teacherDao.insert(contentValues));
                break;
            case TimetableUriMatcher.TIMETABLE_COLLECTION:
                insertedItemUri = appendToContentUri(Metadata.Timetable.CONTENT_URI,
                        timetableDao.insert(contentValues));
                break;
            case TimetableUriMatcher.SUBJECT_COLLECTION:
                insertedItemUri = appendToContentUri(Metadata.Subject.CONTENT_URI,
                        subjectDao.insert(contentValues));
                break;
            case TimetableUriMatcher.LESSON_COLLECTION:
                insertedItemUri = appendToContentUri(Metadata.Lesson.CONTENT_URI,
                        lessonDao.insert(contentValues));
                break;
            default: throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(insertedItemUri, null);
        return insertedItemUri;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        switch (TimetableUriMatcher.matchUri(uri)) {
            case TimetableUriMatcher.TEACHER_COLLECTION:
                return teacherDao.delete(where, whereArgs);
            case TimetableUriMatcher.TIMETABLE_COLLECTION:
                return timetableDao.delete(where, whereArgs);
            case TimetableUriMatcher.SUBJECT_COLLECTION:
                return subjectDao.delete(where, whereArgs);
            case TimetableUriMatcher.LESSON_COLLECTION:
                return lessonDao.delete(where, whereArgs);
            default: throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String where, String[] whereArgs) {
        switch (TimetableUriMatcher.matchUri(uri)) {
            case TimetableUriMatcher.TEACHER_COLLECTION:
                return teacherDao.update(contentValues, where, whereArgs);
            case TimetableUriMatcher.TIMETABLE_COLLECTION:
                return timetableDao.update(contentValues, where, whereArgs);
            case TimetableUriMatcher.SUBJECT_COLLECTION:
                return subjectDao.update(contentValues, where, whereArgs);
            case TimetableUriMatcher.LESSON_COLLECTION:
                return lessonDao.update(contentValues, where, whereArgs);
            default: throw new IllegalArgumentException("Unknown URI: " + uri);
        }
    }
}

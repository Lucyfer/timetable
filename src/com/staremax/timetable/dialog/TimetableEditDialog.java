package com.staremax.timetable.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;
import com.staremax.R;
import com.staremax.timetable.activity.TimetableItemActivity;
import com.staremax.timetable.constants.ActivityConstants;
import com.staremax.timetable.helper.ContentChangeObserver;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.dao.TimetableDao;

public class TimetableEditDialog implements DialogInterface.OnClickListener {
    private final Context context;
    private final long timetableItemId;
    private final AlertDialog.Builder builder;

    public TimetableEditDialog(Context context, long itemId) {
        this.context = context;
        this.timetableItemId = itemId;

        String EDIT_LABEL = context.getResources().getString(R.string.edit);
        String DELETE_LABEL = context.getResources().getString(R.string.delete);
        this.builder = new AlertDialog.Builder(context);
        this.builder.setTitle(R.string.chooseAction);
        this.builder.setItems(new String[]{EDIT_LABEL, DELETE_LABEL}, this);
        this.builder.setNegativeButton(R.string.cancel, null);
        this.builder.create();
    }

    public void show() {
        this.builder.show();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int userChoice) {
        switch (userChoice) {
            case 0:
                Intent editTimetableEntry = new Intent(context, TimetableItemActivity.class);
                editTimetableEntry.putExtra("id", timetableItemId);
                ((Activity) context).startActivityForResult(editTimetableEntry, ActivityConstants.EDIT_TIMETABLE_REQUEST);
                break;
            case 1:
                TimetableDao dao = new TimetableDao(DatabaseHelperFactory.getHelper(context));
                if (dao.deleteById(timetableItemId) <= 0)
                    Toast.makeText(context, R.string.errorOccurred, Toast.LENGTH_SHORT).show();
                else
                    ContentChangeObserver.getInstance().notifyObservers();
                break;
        }
    }
}

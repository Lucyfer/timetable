package com.staremax.timetable.constants;

public interface ActivityConstants {
    int CREATE_TIMETABLE_REQUEST = 1;
    int EDIT_TIMETABLE_REQUEST = 2;
}

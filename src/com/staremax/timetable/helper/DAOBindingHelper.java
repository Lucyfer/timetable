package com.staremax.timetable.helper;

import android.database.Cursor;

public class DAOBindingHelper {
    private DAOBindingHelper() {}

    public static int bindInt(Cursor c, String field) {
        return c.getInt(c.getColumnIndex(field.toLowerCase()));
    }

    public static long bindLong(Cursor c, String field) {
        return c.getLong(c.getColumnIndex(field.toLowerCase()));
    }

    public static String bindString(Cursor c, String field) {
        return c.getString(c.getColumnIndex(field.toLowerCase()));
    }

    public static String bindStringOption(Cursor c, String field) {
        final int index = c.getColumnIndex(field.toLowerCase());
        if (index >= 0)
            return c.getString(index);
        return null;
    }
}

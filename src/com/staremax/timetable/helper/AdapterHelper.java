package com.staremax.timetable.helper;

import android.widget.Adapter;

final class AdapterHelper {
    public static int getAdapterPositionById(final Adapter adapter, final long id) {
        final int count = adapter.getCount();
        for (int pos = 0; pos < count; pos++) {
            if (id == adapter.getItemId(pos)) {
                return pos;
            }
        }
        return 0;
    }
}

package com.staremax.timetable.helper;

import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class ContentChangeObserver extends Observable {
    private static ContentChangeObserver helper = null;
    private Set<Observer> observers;

    public static ContentChangeObserver getInstance() {
        if (helper == null)
            helper = new ContentChangeObserver();
        return helper;
    }

    private ContentChangeObserver() {
        this.observers = new HashSet<Observer>(2);
    }

    @Override
    public void addObserver(Observer observer) {
        this.observers.add(observer);
    }

    @Override
    public synchronized void deleteObserver(Observer observer) {
        this.observers.remove(observer);
    }

    @Override
    public synchronized void deleteObservers() {
        this.observers.clear();
    }

    @Override
    public void notifyObservers() {
        for (Observer o : this.observers) {
            o.update(this, null);
        }
    }
}

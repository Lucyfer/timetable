package com.staremax.timetable.helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.staremax.timetable.provider.dao.LessonDao;
import com.staremax.timetable.provider.dao.SubjectDao;
import com.staremax.timetable.provider.dao.TeacherDao;
import com.staremax.timetable.provider.dao.TimetableDao;

public abstract class DatabaseHelperFactory {
    protected static final String DATABASE_NAME = "timetable.db";
    protected static final int DATABASE_VERSION = 1;

    public static SQLiteOpenHelper getHelper(Context context) {
        return new SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
                @Override
                public void onCreate(SQLiteDatabase database) {
                    LessonDao lessonDao = new LessonDao(null);
                    lessonDao.createTable(database);
                    lessonDao.bootstrap(database);
                    new SubjectDao(null).createTable(database);
                    new TeacherDao(null).createTable(database);
                    new TimetableDao(null).createTable(database);
                }

                @Override
                public void onUpgrade(SQLiteDatabase database, int i, int j) {
                    onCreate(database);
                }
            };
    }
}

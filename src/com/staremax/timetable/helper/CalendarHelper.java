package com.staremax.timetable.helper;

import android.content.Context;
import android.preference.PreferenceManager;

import java.util.Calendar;

public final class CalendarHelper {
    private CalendarHelper() {}

    /**
     * Convert Monday -> 0, ..., Sunday -> 6
     */
    private static int HumanizeDayOfWeek(int d) {
        switch (d) {
            case Calendar.MONDAY:   return 0;
            case Calendar.TUESDAY:  return 1;
            case Calendar.WEDNESDAY:return 2;
            case Calendar.THURSDAY: return 3;
            case Calendar.FRIDAY:   return 4;
            case Calendar.SATURDAY: return 5;
            default: return 6;
        }
    }

    public static int getCurrentDay() {
        Calendar today = Calendar.getInstance();
        return HumanizeDayOfWeek(today.get(Calendar.DAY_OF_WEEK));
    }

    public static int getCurrentWeek(Context context) {
        Calendar today = Calendar.getInstance();
        Calendar september1 = Calendar.getInstance();
        september1.set(today.get(Calendar.YEAR), Calendar.SEPTEMBER, 1);
        boolean even = (today.get(Calendar.WEEK_OF_YEAR) - september1.get(Calendar.WEEK_OF_YEAR)) % 2 == 0;
        boolean iShifted = PreferenceManager.getDefaultSharedPreferences(context).getBoolean("TimetableShift", false);
        return (iShifted ? !even : even) ? 0 : 1;
    }
}

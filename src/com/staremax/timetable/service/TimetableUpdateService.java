package com.staremax.timetable.service;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.database.Cursor;
import android.os.IBinder;
import android.text.TextUtils;
import android.widget.RemoteViews;
import com.staremax.R;
import com.staremax.timetable.helper.CalendarHelper;
import com.staremax.timetable.helper.DatabaseHelperFactory;
import com.staremax.timetable.provider.dao.TimetableDao;
import com.staremax.timetable.provider.iterator.PojoIterator;
import com.staremax.timetable.provider.mapper.TimetableMapper;
import com.staremax.timetable.provider.pojo.Timetable;

public class TimetableUpdateService extends Service {
    public static final String UPDATE_TIMETABLE = "UpdateTimeTable";

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        update(intent);
        stopSelf(startId);
        return START_STICKY;
    }

    private void update(Intent intent) {
        if (intent != null) {
            String requestedAction = intent.getAction();
            if (requestedAction != null && requestedAction.equals(UPDATE_TIMETABLE)) {
                final int widgetId = intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
                final AppWidgetManager appWidgetMan = AppWidgetManager.getInstance(getApplicationContext());
                final RemoteViews views = new RemoteViews(getApplicationContext().getPackageName(), R.layout.widget);
                final StringBuilder sb = new StringBuilder(100);
                final String separator = " | ";
                final TimetableDao dao = new TimetableDao(DatabaseHelperFactory.getHelper(getApplicationContext()));
                final Cursor timetableCursor = dao.selectJoined(
                        CalendarHelper.getCurrentWeek(getApplicationContext()), CalendarHelper.getCurrentDay());
                final PojoIterator<Timetable> timetableIterator = new PojoIterator<Timetable>(
                        timetableCursor, TimetableMapper.INSTANCE);
                for (Timetable model : timetableIterator) {
                    sb.append(model.lesson.beginTime).append(separator).append(model.subject.title);
                    if (!TextUtils.isEmpty(model.auditorium))
                        sb.append(" [").append(model.auditorium).append(']');
                    sb.append('\n');
                }
                if (timetableCursor.getCount() == 0)
                    views.setTextViewText(R.id.widgetText, getResources().getString(R.string.empty));
                else
                    views.setTextViewText(R.id.widgetText, sb.toString());
                appWidgetMan.updateAppWidget(widgetId, views);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
